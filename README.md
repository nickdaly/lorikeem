LorikeeM
========

LorikeeM MUMPS Developer Tools for Emacs

This major mode for emacs provides basic syntax highlighting for MUMPS.
It supports most features of YottaDB and Fidelity GT.M.

### License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License (AGPL)
as published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

### Installation

1. Symlink src/lorikeem/lorikeem.el to your load-path as mumps-mode.el.

       $ mkdir -p ~/.emacs.d/packages/lorikeem
       $ ln -s ~/lorikeem/src/lorikeem/lorikeem.el ~/.emacs.d/packages/lorikeem/mumps-mode.el

   Renaming LorikeeM allows normal LorkieeM and this simplified mumps-mode to coexist.

2. Teach Emacs about mumps-mode and load it.

       $ echo "(add-to-list 'load-path \"~/.emacs.d/packages/lorikeem\")" >> ~/.emacs.d/init.el
       $ echo "(require 'mumps-mode)" >> ~/.emacs.d/init.el

### Credits

KBAWDUMP.m and mktags were written by DL Wicksell for his excellent
Axiom tools for the vim editor. https://github.com/dlwicksell/axiom

### Changelog

- 2020-02-29: Unhook the intrusive bits of LorikeeM to produce a
  simple mumps-mode that does little more than fontlocking, by
  default. The rest of the bits are still there and are available to
  users, as necessary.
